import java.lang.*;
import java.util.stream.LongStream;

public class Factorial {

  private static long multRange(long from, long to) {
    return LongStream.rangeClosed(from, to)
                     .reduce(1, Math::multiplyExact);
  }

  private static long factorial(long n) {
    if(n <= 1) {
      return 1;
    }
    return n*factorial(n - 1);
  }

  private static long binom(long n, long k) {
    if(0 <= k && k <= n) {
      return multRange(n - k + 1, n) / factorial(k);
    }
    return 0;
  }

  private static long lotto(long n, long k) {
    return 0 <= k && k <= n ? multRange(n - k + 1, n) : 0;
    // return binom(n, k) * factorial(k);
  }

  public static void main(String[] args) {
    long n = Long.parseLong(args[0]);
    long k = Long.parseLong(args[1]);
    System.out.println("The factorial of " + k + " is " + factorial(k) + ".");
    System.out.println(n + " choose " + k + " is " + binom(n, k) + ".");
    System.out.println("Lotto by choosing " + k + " numbers out of " + n + " makes " + lotto(n, k) + " possibilities.");
  }
}
