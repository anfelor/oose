public class ShippingAuthority {
  private Ship[] ships = new Ship[50];
  private int registered = 0;

  public void register(Ship s) {
    ships[registered++] = s;
  }

  public void notify(String msg, int urgency) {
    if(urgency == 1) {
      for(Ship s : ships) {
        if(s != null) s.receiveMessage(msg);
      }
    }
  }

  public static String getLicense() {
    java.util.Random rnd = new java.util.Random();
    StringBuilder sb = new StringBuilder();
    for(int i = 0; i < 6; i++) {
      sb.append((char)((rnd.nextInt(25)+65)));
    }
    return sb.toString();
  }
}
