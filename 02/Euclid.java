public class Euclid {
  private static long gcd(long a, long b) {
    if(a == 0) return b;
    while(b != 0) {
      if(a > b) {
        a = a - b;
      } else {
        b = b - a;
      }
    }
    return a;
  }

  public static void main(String[] args) {
    long a = Long.parseLong(args[0]);
    long b = Long.parseLong(args[1]);
    System.out.println("gcd(" + a + ", " + b + ") = " + gcd(a,b));
  }
}
