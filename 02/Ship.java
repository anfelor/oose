public class Ship {
  private String name;
  private String license;
  private double length;

  public Ship(String name, double length) {
    this.name = name;
    this.length = length;
  }

  public void setLicense(String license) {
    this.license = license;
  }

  public void receiveMessage(String msg) {
    System.out.println("Ship " + name + " registered under " + license +  " received: " + msg);
  }
}
