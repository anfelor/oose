public class Shipyard {
  public void craft(String name, double length, ShippingAuthority auth) {
    Ship s = new Ship(name, length);
    auth.register(s);
    s.setLicense(ShippingAuthority.getLicense());
  }

  public static void main(String[] args) {
    ShippingAuthority auth = new ShippingAuthority();
    Shipyard w1 = new Shipyard();
    w1.craft("Gertrut", 12.5, auth);
    Ship sc1 = new Ship("Merlin", 45.9);
    auth.register(sc1);
    sc1.setLicense(ShippingAuthority.getLicense());
    auth.notify("Havarie im Rhein bei Rheinkilometer 591.", 1);
    //Ausgabe Empfangen von Gertrut/IHVMXW:  Havarie im Rhein bei Rheinkilometer 591.
    //Empfangen von Merlin/VPBJQW:  Havarie im Rhein bei Rheinkilometer 591.
    auth.notify("Sonnenschein bei Rheinkilometer 650", 0);
  }
}
