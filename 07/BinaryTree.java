import java.util.function.Function;
import java.util.function.Consumer;
import java.util.LinkedList;
import java.util.ListIterator;

class TNode { 
  private int id;
  public TNode(int id) {
    this.id = id;
  }
  public int getId() {
    return id;
  }
}

public class BinaryTree<T> {
  private T node;
  private BinaryTree<T> left;
  private BinaryTree<T> right;
  public BinaryTree(T node) {
    this(node, null, null);
  }
  public BinaryTree(T node, BinaryTree<T> left, BinaryTree<T> right) {
    this.node = node;
    this.left = left;
    this.right = right;
  }
  public <R> BinaryTree<R> inOrder(Function<T, R> c) {
    BinaryTree<R> l = null, r = null; 
    if(left != null) l = left.inOrder(c);
    R n = c.apply(node);
    if(right != null) r = right.inOrder(c);
    return new BinaryTree<R>(n, l, r);
  }
  public <R> BinaryTree<R> preOrder(Function<T, R> c) {
    BinaryTree<R> l = null, r = null;
    R n = c.apply(node);
    if(left != null) l = left.preOrder(c);
    if(right != null) r = right.preOrder(c);
    return new BinaryTree<R>(n, l, r);
  }
  public <R> BinaryTree<R> postOrder(Function<T, R> c) {
    BinaryTree<R> l = null, r = null; 
    if(left != null) l = left.postOrder(c);
    if(right != null) r = right.postOrder(c);
    R n = c.apply(node);
    return new BinaryTree<R>(n, l, r);
  }
  public void levelOrder(Consumer<LinkedList<T>> c) {
    LinkedList<LinkedList<T>> levels = levelOrderedList();
    ListIterator<LinkedList<T>> li = levels.listIterator();
    while(li.hasNext()) {
      c.accept(li.next());
    }
  }
  private LinkedList<LinkedList<T>> levelOrderedList() {
    LinkedList<LinkedList<T>> l = new LinkedList<LinkedList<T>>();
    LinkedList<LinkedList<T>> r = new LinkedList<LinkedList<T>>();
    if(left != null) l = left.levelOrderedList();
    if(right != null) r = right.levelOrderedList();
    ListIterator<LinkedList<T>> li = l.listIterator();
    ListIterator<LinkedList<T>> ri = r.listIterator();
    while(ri.hasNext()) {
      LinkedList<T> rt = ri.next();
      LinkedList<T> lt = new LinkedList<T>();
      if(li.hasNext()) {
        lt = li.next();
        lt.addAll(rt);
      } else {
        li.add(rt);
      }
    }
    l.add(0, new LinkedList<T>());
    l.get(0).add(node);
    return l;
  }
  public static void main(String args[]) {
    BinaryTree<Integer> b = new BinaryTree(10
        , new BinaryTree(5, new BinaryTree(3, null, new BinaryTree(4))
                        , new BinaryTree(7))
        , new BinaryTree(12, null, new BinaryTree(15, new BinaryTree(13), null)));
    BinaryTree<TNode> t = b.inOrder((Integer node) -> new TNode(node));
    t.inOrder((TNode n) -> { System.out.println(n.getId()); return 1; });
    t.preOrder((TNode n) -> { System.out.println(n.getId()); return 1; });
    t.postOrder((TNode n) -> { System.out.println(n.getId()); return 1; });
    t.levelOrder((LinkedList<TNode> l) -> {
      ListIterator<TNode> li = l.listIterator();
      while(li.hasNext()) {
        System.out.print(li.next().getId() + " ");
      }
      System.out.println();
    });
  }
}
