import java.io.File;

public class DateiBeobachter implements Runnable {
  private File f;
  private long lastModified;
  public DateiBeobachter(String file) {
    f = new File(file);
    lastModified = f.lastModified();
  }
  public DateiBeobachter(java.io.File file) {
    f = file;
    lastModified = f.lastModified();
  }
  public void run() {
    while(true) {
      System.out.println(f);
      if(f.lastModified() != lastModified) {
        lastModified = f.lastModified();
        System.out.println("Modified!");
      }
      try {
        Thread.sleep(500);
      } catch(InterruptedException e) {};
    }
  }
  public static void main(String args[]) {
    Thread t = new Thread(new DateiBeobachter("hi.txt"));
    t.start();
  }
}
