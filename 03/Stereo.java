public class Stereo {
  public void play(Playable p) {
    p.song();
  }
  public static void main(String[] args) {
    Stereo s = new Stereo();
    Playable p = new CD();
    s.play(p);
    p = new Tape();
    s.play(p);
  }
}
