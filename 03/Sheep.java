public class Sheep implements Cloneable {
 public String name;
 public Fur fur;

 public Sheep(String name, Fur fur) {
  this.name = name;
  this.fur = fur;
 }

 public void shear() {
  fur.length = 0;
 }

 public String toString() {
  return "Name: " + name + " " + fur.toString();
 }

 @Override
 public Sheep clone() {
  return this;
 }

 public Sheep shallowClone() {
   return new Sheep(this.name, this.fur);
 }

 public Sheep deepClone() {
   return new Sheep(this.name, new Fur(this.fur.length));
 }

  @Override
  public boolean equals(Object o) {
      if (o == null || !(o instanceof Sheep))
          return false;
      Sheep s = (Sheep) o;
      return name == s.name && fur.equals(s.fur);
  }
}
