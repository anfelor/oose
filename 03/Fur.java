public class Fur {
 public int length;

 public Fur(int length) {
  this.length = length;
 }

 public String toString() {
  return "Felllaenge: " + length;
 }

  @Override
  public boolean equals(Object o) {
      if (o == null || !(o instanceof Fur))
          return false;
      Fur f = (Fur) o;
      return length == f.length;
  }
}
