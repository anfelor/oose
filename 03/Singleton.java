public class Singleton {
  private static Singleton s;
  private Singleton() {}
  public static Singleton getMe() {
    if(s == null) s = new Singleton();
    return s;
  }
}
