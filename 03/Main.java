public class Main {
  public static void main(String[] args) {
    Sheep m = new Sheep("Molly", new Fur(10));
    Sheep d = m.shallowClone(); 
    System.out.println(m.equals(d));
    d.name = "Dolly";

    System.out.println(m.toString());
    System.out.println(d.toString());
    d.shear();
    System.out.println(m.toString());
    System.out.println(d.toString());

    Sheep s = new Sheep("Shirley", new Fur(10));
    Sheep t = s.deepClone(); 
    System.out.println(s.equals(t));
    t.name = "Timmy";

    System.out.println(s.toString());
    System.out.println(t.toString());
    t.shear();
    System.out.println(s.toString());
    System.out.println(t.toString());
  }
}
