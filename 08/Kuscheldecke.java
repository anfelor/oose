public class Kuscheldecke<E> {
  private E eingekuscheltesWesen;
  public void setWesen(E x) {
    eingekuscheltesWesen = x;
  }
  public E getWesen() {
    return eingekuscheltesWesen;
  }
  public static void main(String args[]) {
    //Kuscheldecke<Tier> decke1 = new Kuscheldecke<Katze>();
    //Kuscheldecke<Hund> decke2 = new Kuscheldecke<Tier>();
    //Kuscheldecke<?> decke3 = new Kuscheldecke<Katze>();
    //decke3.setWesen(new Katze());
    Kuscheldecke decke4 = new Kuscheldecke();
    decke4.setWesen(new Hund());
  }
}

class Tier {}
class Hund extends Tier {}
class Katze extends Tier {}
