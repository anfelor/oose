import java.util.List;

interface Stack<T> extends List<T> {
  public void push(T e);
  public T pop(T e);
}
