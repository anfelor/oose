class Person implements Comparable<Person> {
  public String name;
  public String vorname;
  public int postleitzahl;
  public String strasse;
  public int hausnummer;

  public int compareTo(Person p) {
    if(p == null)
      throw new NullPointerException();
    if(name.compareTo(p.name) != 0)
      return name.compareTo(p.name);
    if(vorname.compareTo(p.vorname) != 0)
      return vorname.compareTo(p.vorname);
    if(postleitzahl != p.postleitzahl)
      return postleitzahl < p.postleitzahl ? - 1 : 1;
    if(strasse.compareTo(p.strasse) != 0)
      return strasse.compareTo(p.strasse);
    if(hausnummer != p.hausnummer)
      return hausnummer < p.hausnummer ? - 1 : 1;
    return 0;
  }
}
