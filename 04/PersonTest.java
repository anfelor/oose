class PersonTest {
  public static Comparable findMin1(Comparable[] x) {
    if(x == null) throw new NullPointerException();
    if(x.length == 0) throw new IndexOutOfBoundsException();

    Comparable min = x[0];
    for(int i = 1; i < x.length; ++i) {
      if(x[i].compareTo(min) < 0) {
        min = x[i];
      }
    }
    return min;
  }

  public static <T extends Comparable<T>> T findMin2(T[] x) {
    if(x == null) throw new NullPointerException();
    if(x.length == 0) throw new IndexOutOfBoundsException();

    T min = x[0];
    for(int i = 1; i < x.length; ++i) {
      if(x[i].compareTo(min) < 0) {
        min = x[i];
      }
    }
    return min;
  }

  public static void main(String[] args) {
    Person p1 = new Person() {{
      name = "Lorenzen"; vorname = "Anton"; postleitzahl = -1;
      strasse = "Stairway to heaven"; hausnummer = 42;
    }};
    Person p2 = new Person() {{
      name = "Gollum"; vorname = "Gollum"; postleitzahl = -1;
      strasse = "A cave deep below.."; hausnummer = 1;
    }};
    Person[] ps = {p1, p2};
    Integer[] as = {234532, 346712, 238234, 101092, 2134120};
    System.out.println((Integer) findMin1((Comparable<Integer>[]) as));
    Person min = (Person) findMin1((Comparable<Person>[]) ps);
    System.out.println(min.vorname + " " + min.name);

    System.out.println(findMin2(as));
    min = findMin2(ps);
    System.out.println(min.vorname + " " + min.name);
  }
}
