public class Rechner {
  public static void main(String[] args) {
    int option = Integer.parseInt(args[2]);
    int a = Integer.parseInt(args[0]);
    int b = Integer.parseInt(args[1]);
    if(option == 0) {
      System.out.println("Summe: " + (a + b));
    } else if(option == 1) {
      System.out.println("Differenz: " + (a - b));
    } else if(option == 2) {
      System.out.println("Produkt: " + (a * b));
    } else if(option == 3) {
      if(b == 0) {
        System.out.println("Man kann nicht durch 0 teilen!");
      } else {
        System.out.println("Quotient: " + (a / (double) b));
      }
    }
  }
}
